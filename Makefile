uti=src/utils/
gpg=src/gpg/
gtk=src/gtk/

obj=obj/

opt= -g

forall=$(obj)main.o $(obj)filemanager.o $(obj)keysmanager.o $(obj)aes256.o $(obj)signing.o $(obj)linker.o $(obj)windowui.o $(obj)dialogs.o $(obj)errors.o

all : gpgencrypt

gpgencrypt : $(forall)
	gcc -Wall $(opt) $(forall) -o gpgencrypt -lgcrypt `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0`

$(obj)filemanager.o : $(uti)filemanager.c $(uti)errors.h
	gcc -Wall $(opt) -c $(uti)filemanager.c -o $(obj)filemanager.o

$(obj)errors.o : $(uti)errors.c
	gcc -Wall $(opt) -c $(uti)errors.c -o $(obj)errors.o

$(obj)aes256.o : $(gpg)aes256.c $(uti)errors.h
	gcc -Wall $(opt) -c $(gpg)aes256.c -o $(obj)aes256.o -lgcrypt

$(obj)signing.o : $(gpg)signing.c $(uti)errors.h
	gcc -Wall $(opt) -c $(gpg)signing.c -o $(obj)signing.o -lgcrypt

$(obj)keysmanager.o : $(gpg)keysmanager.c $(uti)errors.h
	gcc -Wall $(opt) -c $(gpg)keysmanager.c -o $(obj)keysmanager.o -lgcrypt

$(obj)windowui.o : $(gtk)windowui.c
	gcc -Wall $(opt) -c $(gtk)windowui.c -o $(obj)windowui.o `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0`

$(obj)dialogs.o : $(gtk)dialogs.c
	gcc -Wall $(opt) -c $(gtk)dialogs.c -o $(obj)dialogs.o `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0`

$(obj)linker.o : src/linker.c $(gtk)windowui.h $(uti)filemanager.h $(gpg)aes256.h $(gpg)keysmanager.h $(uti)errors.h
	gcc -Wall $(opt) -c src/linker.c -o $(obj)linker.o `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0`

$(obj)main.o : src/main.c src/linker.h $(uti)filemanager.h $(gpg)aes256.h
	gcc -Wall $(opt) -c src/main.c -o $(obj)main.o `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0`

clean :
	rm -rf $(obj)*.o gpgencrypt
