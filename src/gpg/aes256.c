#include "aes256.h"

size_t get_nbr_blocsaes256(size_t len) {
	return len / AES256_BLOCK_SIZE + ((len % AES256_BLOCK_SIZE == 0)?0:1 );
}

void lance_processus_aes256(gcry_cipher_hd_t * handle, void * cle, char * matrice, int * error) {
	*error = ERROR_NO;

	// On stockera les erreurs ici
	gcry_error_t error_encrypt;

	// On cree notre cipher
	error_encrypt = gcry_cipher_open(handle,
                         GCRY_CIPHER_AES256,
                         GCRY_CIPHER_MODE_CBC,
                         GCRY_CIPHER_CBC_CTS);
	if(error_encrypt > 0) {
		// S'il y a eu une erreur
		printf("Fail to open cipher : %s\n", gcry_strerror(error_encrypt));
		*error = ERROR_CANNOT_START_PROCESS;
		return;
	}

	// On ajoute une cle a notre cipher
	error_encrypt = gcry_cipher_setkey(*handle, cle, AES256_KEY_SIZE);
	if(error_encrypt > 0) {
		// S'il y a eu une erreur
		printf("Fail to set cipher key : %s\n", gcry_strerror(error_encrypt));
		*error = ERROR_CANNOT_USE_KEY;
		return;
	}

	// On ajoute une matrice a notre cypher
	error_encrypt = gcry_cipher_setiv(*handle, matrice, AES256_BLOCK_SIZE);
	if(error_encrypt > 0) {
		// S'il y a eu une erreur
		printf("Fail to set cipher matrice : %s\n", gcry_strerror(error_encrypt));
		*error = ERROR_CANNOT_USE_MATRIX;
		return;
	}
}

void decryptage_aes256(gcry_cipher_hd_t handle, char * texte_crypte, size_t texte_crypte_len, char * buffer, size_t * buffer_len, int * error) {
	*error = ERROR_NO;

	// On verifie que le buffer soit assez grand
	if(texte_crypte_len > *buffer_len) {
		printf("Need more space to decrypt\n");
		exit(1);
	}
	// Le decryptage se fait en place, pour ne pas ecraser le texte_crypte, on le copie dans buffer
	memcpy(buffer, texte_crypte, texte_crypte_len);

	// On decrypte le contenu de buffer
	gcry_error_t error_encrypt = gcry_cipher_decrypt(handle, buffer, texte_crypte_len, NULL, 0);
	if(error_encrypt > 0) {
		// S'il y a eu une erreur
		printf("Fail to encrypt text : %s\n", gcry_strerror(error_encrypt));
		*error = ERROR_CANNOT_DECRYPT;
		return;
	}
}

size_t encryptage_aes256(gcry_cipher_hd_t handle, char * text_clair, size_t text_clair_len, char ** buffer, int * error) {
	*error = ERROR_NO;
	
	// On calcule le nombre de blocs d'AES necessaire pour le cryptage
	size_t nbr_blocs = get_nbr_blocsaes256(text_clair_len);

	// Le cryptage se fait en place, pour ne pas ecraser le text_clair, on le copie dans *buffer
	size_t size = nbr_blocs * AES256_BLOCK_SIZE;
	*buffer = malloc(size); // On alloue de la memoire pour la copie
	memset(*buffer, 0, size);
	if(*buffer == NULL) {
		// S'il y a eu une erreur d'allocation de memoire
		printf("Fail to allocate memory for texte_crypte\n");
		*error = ERROR_CANNOT_ENCRYPT;
		return 0;
	}
	memcpy(*buffer, text_clair, size); // On copie

	// On encrypte
	gcry_error_t error_encrypt = gcry_cipher_encrypt(handle, *buffer, size, NULL, 0);
	if(error_encrypt > 0) {
		// S'il y a eu une erreur
		printf("Fail to encrypt text : %s\n", gcry_strerror(error_encrypt));
		*error = ERROR_CANNOT_ENCRYPT;
		return 0;
	}

	return size;
}