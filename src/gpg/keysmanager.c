#include "keysmanager.h"

void * generate_cle(char * password, size_t cle_len, char * salt, size_t salt_size, int * error) {
	*error = ERROR_NO;
	
	void * cle = malloc(cle_len); // Le buffer où stocker la clé
	gpg_error_t error_key_gen = gcry_kdf_derive(
							password, // Le mot de passe
							strlen(password), // La taille du mot de passe
							GCRY_KDF_PBKDF2, // Algorithme de derivation de cle utilise
							GCRY_MD_SHA512, // Le sous algorithme utilisé
							salt, // Le paramètre salt
							salt_size, // La taille du paramètre salt
							NBR_ITERATIONS, // Le nombre d'itération de l'algorithme
							cle_len, // La taille voulu de la clé
							cle); // L'endroit où stocker la clé
	if(error_key_gen > 0) {
		// S'il y a des erreurs
		printf("Fail to generate key : %s\n", gcry_strerror(error_key_gen));
		*error = ERROR_CANNOT_PROCESS_KEY;
		return NULL;
	}
	return cle;
}