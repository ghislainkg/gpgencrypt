#ifndef _AES_256_
#define _AES_256_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gcrypt.h>

#include "../utils/errors.h"

// La taille de cle utilise pour l'AES256
#define AES256_KEY_SIZE 32

// La taille des blocs a crypte pour l'AES256
#define AES256_BLOCK_SIZE 16

// On peut avoir le nombre de blocs necessaire pour un texte de taille len
size_t get_nbr_blocsaes256(size_t len);

/* On peut initialiser le processus de cryptage / decriptage
Pour cela, on a besoin d'une cle de taille AES256_KEY_SIZE qui servira aux operations de cryptage/decryptage
Et d'une matrice de taille AES256_BLOCK_SIZE
Le cipher de cryptage/decriptage produit sera stocke dans handle
*/
void lance_processus_aes256(gcry_cipher_hd_t * handle, void * cle, char * matrice, int * error);

/* On peut decrypter un texte crypte (texte_crypte) de taille texte_crypte_len
Pour le faire, on a besoin d'un cipher initialise avec le bon algorithme de cryptage (ici AES256)
(On peut utiliser le resultat de lance_processus_aes256 ci dessus)
Le texte decrypte sera stocker dans buffer de taille buffer_len
(Buffer doit etre de taille au moins texte_crypte_len)
*/
void decryptage_aes256(gcry_cipher_hd_t handle, char * texte_crypte, size_t texte_crypte_len, char * buffer, size_t * buffer_len, int * error);

/* On peut encrypter un texte (text_clair) de taille text_clair_len
Pour le faire, on a besoin d'un cipher initialise avec le bon algorithme de cryptage (ici AES256)
(On peut utiliser le resultat de lance_processus_aes256 ci dessus)
Le texte crypter resultant sera stocker dans un pointeur pointe dont l'addresse sera pointe par buffer
(Pour y acceder : char * texte_crypte = *buffer;)
*/
size_t encryptage_aes256(gcry_cipher_hd_t handle, char * text_clair, size_t text_clair_len, char ** buffer, int * error);

#endif