#ifndef _SIGNING_
#define _SIGNING_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gcrypt.h>

#include "../utils/errors.h"

#define SIGN_KEY_SIZE 50

void lancer_processus_signatureverif(gcry_mac_hd_t * mac, void * cle, int * error);

/*hashlen doit avant etre initialiser avec la taille de hash*/
void signer(gcry_mac_hd_t mac, char * data, size_t datalen, char * hash, size_t * hashlen, int * error);

int verifier(gcry_mac_hd_t mac, char * data, size_t datalen, char * hash, size_t hashlen, int * error);

#endif