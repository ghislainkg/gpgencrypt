#ifndef _KEYS_MANAGER_
#define _KEYS_MANAGER_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gcrypt.h>

#include "aes256.h"

#include "../utils/errors.h"

// Ce fichier contient quelques fonctions pour generer des cles necessaire au criptage et au decryptage de fichiers

// Le nombre d'iteration pour la derivation de cle a partir d'un mot de passe
#define NBR_ITERATIONS 50000

// La taille des Salt pour la derivation de cle a partir de mot de passe
#define SALT_SIZE 128

/*
On peut generer une cle pour l'AES256 a partir :
- D'un mot de passe et
- d'un salt
la cle generer sera de taille cle_len
*/
void * generate_cle(char * password, size_t cle_len, char * salt, size_t salt_size_bytes, int * error);

#endif