#include "signing.h"

void lancer_processus_signatureverif(gcry_mac_hd_t * mac, void * cle, int * error) {
	*error = ERROR_NO;

	gcry_error_t err = gcry_mac_open(mac, GCRY_MAC_HMAC_SHA512, 0, NULL);
	if (err) {
		printf("Fail to open the mac : %s\n", gcry_strerror(err));
		*error = ERROR_CANNOT_START_PROCESS;
		return;
	}

	err = gcry_mac_setkey(*mac, cle, SIGN_KEY_SIZE);
	if (err) {
		printf("Fail to set the key : %s\n", gcry_strerror(err));
		*error = ERROR_CANNOT_USE_KEY;
		return;
	}
}

void signer(gcry_mac_hd_t mac, char * data, size_t datalen, char * hash, size_t * hashlen, int *error) {
	*error = ERROR_NO;

	gcry_error_t err = gcry_mac_write(mac, data, datalen);
	if (err) {
		printf(" Fail to write data in mac : %s\n", gcry_strerror(err));
		*error = ERROR_CANNOT_SIGN_DATA;
		return;
	}

	err = gcry_mac_read(mac, hash, hashlen);
	if (err) {
		printf("Fail to sign data : %s\n", gcry_strerror(err));
		*error = ERROR_CANNOT_SIGN_DATA;
		return;
	}
}

int verifier(gcry_mac_hd_t mac, char * data, size_t datalen, char * hash, size_t hashlen, int * error) {
	*error = ERROR_NO;

	gcry_error_t err = gcry_mac_write(mac, data, datalen);
	if (err) {
		printf("Fail to write data in mac : %s\n", gcry_strerror(err));
		*error = ERROR_CANNOT_VERIF_DATA;
		return 0;
	}

	err = gcry_mac_verify(mac, hash, hashlen);
	if(err) {
		if(err == GPG_ERR_CHECKSUM) {
			printf("sign Error 1\n");
			return 0;
		}
		else {
			printf("Fail to check data : %s\n", gcry_strerror(err));
			*error = ERROR_CANNOT_VERIF_DATA;
			printf("sign Error 2\n");
			return 0;
		}
	}
	else {
		printf("sign ok\n");
		return 1;
	}
}