#ifndef _WINDOW_UI_
#define _WINDOW_UI_

#include <gtk/gtk.h>
#include <stdlib.h>
#include <stdio.h>

#define GLADE_UI_FILE_NAME "glade-ui/ui.glade"

/*Definition de ce qu'on peut faire avec l'interface graphique du logiciel*/

/*On peut recevoir des donnees de l'interface graphique lorsque certain evenement y occurre
un CipherArguments represente les entrees de l'utilisateur*/
typedef struct CipherArguments {
	char * password;
	size_t password_len;
	char * filenamesrc;
	size_t filenamesrc_len;
	char * filenameres;
	size_t filenameres_len;

	GtkWindow * window;
	GtkApplication * app;
} CipherArguments;

/*Les donnees recues lors d'evenement sont envoyees par les fonctions
d'un WindowFunctionsHolder qui sont executees lors de ces evenements */
typedef struct WindowFunctionsHolder {
	void (*on_encrypt_clicked) (CipherArguments); // Click sur le bouton d'encryptage
	void (*on_decrypt_clicked) (CipherArguments); // Click sur le bouton de decryptage
	void (*on_sign_clicked) (CipherArguments);
	void (*on_check_clicked) (CipherArguments);
} WindowFunctionsHolder;

/*Quelques fonctions pour la gestions des WindowFunctionsHolder*/
WindowFunctionsHolder * create_window_functions();
void window_functions_set_on_encrypt_clicked(WindowFunctionsHolder * window_functions, void (*on_encrypt_clicked) ());
void window_functions_set_on_decrypt_clicked(WindowFunctionsHolder * window_functions, void (*on_decrypt_clicked) ());
void window_functions_set_on_sign_clicked(WindowFunctionsHolder * window_functions, void (*on_sign_clicked) ());
void window_functions_set_on_check_clicked(WindowFunctionsHolder * window_functions, void (*on_check_clicked) ());

/*On peut creer une fenetre qui executera les fonctions de functions lors des evenements associes*/
int create_gtk_app(WindowFunctionsHolder * functions, int argc, char ** argv);

//---------------------------------------------------------------------------------------------------------------

void create_main_window(GtkApplication * app, WindowFunctionsHolder * functions);

/*Fonctions et types utilisees pour l'appel des fonctions des WindowFunctionsHolder
lors des evenements*/
typedef struct CipherAuxFunctionsArgument {
	WindowFunctionsHolder * functions;
	GtkEntry * filenamesrc_entry;
	GtkEntry * filenameres_entry;
	GtkEntry * password_entry;

	GtkWindow * window;
	GtkApplication * app;
} CipherAuxFunctionsArgument;

void on_encrypt_clicked_aux(CipherAuxFunctionsArgument * args);    
void on_decrypt_clicked_aux(CipherAuxFunctionsArgument * args);
void on_sign_clicked_aux(CipherAuxFunctionsArgument * args);
void on_check_clicked_aux(CipherAuxFunctionsArgument * args);

/*-----------------------------------*/
void close_app();

#endif