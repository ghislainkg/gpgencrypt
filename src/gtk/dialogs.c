#include "dialogs.h"



void open_dialog_file_new(OpenDialogArguments * args) {
    GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_SAVE;
    open_dialog_file(args, action);
}

void open_dialog_file_open(OpenDialogArguments * args) {
    GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_OPEN;
    open_dialog_file(args, action);   
}

void open_dialog_file(OpenDialogArguments * args, GtkFileChooserAction action) {
    GtkWindow * window = GTK_WINDOW(args->window);
    GtkEntry * entry = GTK_ENTRY(args->entry);
    
    gchar * response_cancel = "Cancel";
    gint response_cancel_id = 1;
    gchar * response_ok = "Choisir";
    gint response_ok_id = 2;
    GtkWidget * dialog = gtk_file_chooser_dialog_new(
        "Choisir un fichier"
        ,window
        ,action
        ,response_cancel, response_cancel_id
        ,response_ok, response_ok_id
        ,NULL
    );

    gint response = gtk_dialog_run(GTK_DIALOG(dialog));
    if(response == response_ok_id) {
        GtkFileChooser * file_chooser = GTK_FILE_CHOOSER(dialog);
        char * filename;
        filename = gtk_file_chooser_get_filename(file_chooser);
        gtk_entry_set_text(entry, filename);
        g_free(filename);
    }

    gtk_widget_destroy(GTK_WIDGET(dialog));
}


void open_dialog_message_default(GtkWindow * parent, char * titre, char * message, void (*on_ok)(), void (*on_annuler)()) {
	open_dialog_message(parent, titre, message, "ok", on_ok, "Annuler", on_annuler);
}

void open_dialog_message(GtkWindow * parent, char * titre, char * message, char * ok_label, void (*on_ok)(), char * annuler_label, void (*on_annuler)()) {
    GtkWidget *dialog, *label, *content_area;
    GtkDialogFlags flags;

    gchar * response_ok = ok_label;
    gint  response_ok_id = 10;
    gchar * response_annuler = annuler_label;
    gint response_annuler_id = 22;

    flags = GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL;
    dialog = gtk_dialog_new_with_buttons(
        titre,
        parent,
        flags,
        response_ok, response_ok_id,
        response_annuler, response_annuler_id,
        NULL
    );

    content_area = gtk_dialog_get_content_area(GTK_DIALOG (dialog));
    
    label = gtk_label_new (message);

    gtk_container_add(GTK_CONTAINER (content_area), label);
    gtk_widget_show_all(dialog);

    gint response = gtk_dialog_run(GTK_DIALOG(dialog));

    if(response == response_ok_id) {
        if(on_ok != NULL) {
            on_ok();
        }
    }
    else if (response == response_annuler_id) {
        if(on_annuler != NULL) {
            on_annuler();
        }
    }

    gtk_widget_destroy(dialog);
}