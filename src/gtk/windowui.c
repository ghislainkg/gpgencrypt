
#include "windowui.h"
#include "dialogs.h"

WindowFunctionsHolder * create_window_functions() {
    WindowFunctionsHolder * res = malloc(sizeof(WindowFunctionsHolder));
    res->on_encrypt_clicked = NULL;
    res->on_decrypt_clicked = NULL;
    res->on_sign_clicked = NULL;
    res->on_check_clicked = NULL;
    return res;
}
void window_functions_set_on_encrypt_clicked(WindowFunctionsHolder * window_functions, void (*on_encrypt_clicked) ()) {
    window_functions->on_encrypt_clicked = on_encrypt_clicked;
}
void window_functions_set_on_decrypt_clicked(WindowFunctionsHolder * window_functions, void (*on_decrypt_clicked) ()) {
    window_functions->on_decrypt_clicked = on_decrypt_clicked;
}
void window_functions_set_on_sign_clicked(WindowFunctionsHolder * window_functions, void (*on_sign_clicked) ()) {
    window_functions->on_sign_clicked = on_sign_clicked;
}
void window_functions_set_on_check_clicked(WindowFunctionsHolder * window_functions, void (*on_check_clicked) ()) {
    window_functions->on_check_clicked = on_check_clicked;
}

CipherArguments get_cipher_arguments_from_args(CipherAuxFunctionsArgument * args) {
    GtkEntry * filenamesrc_entry = args->filenamesrc_entry;
    GtkEntry * filenameres_entry = args->filenameres_entry;
    GtkEntry * password_entry = args->password_entry;

    CipherArguments cipher_args;
    memset(&cipher_args, 0, sizeof(CipherArguments));
    
    cipher_args.password = (char*) gtk_entry_get_text(password_entry);
    cipher_args.password_len = gtk_entry_get_text_length(password_entry);
    
    cipher_args.filenamesrc = (char*) gtk_entry_get_text(filenamesrc_entry);
    cipher_args.filenamesrc_len = gtk_entry_get_text_length(filenamesrc_entry);
    
    cipher_args.filenameres = (char*) gtk_entry_get_text(filenameres_entry);
    cipher_args.filenameres_len = gtk_entry_get_text_length(filenameres_entry);

    cipher_args.window = args->window;
    cipher_args.app = args->app;

    return cipher_args;
}

void on_encrypt_clicked_aux(CipherAuxFunctionsArgument * args) {
    CipherArguments cipher_args = get_cipher_arguments_from_args(args);
    WindowFunctionsHolder * functions = args->functions;
    functions->on_encrypt_clicked(cipher_args);
}
void on_decrypt_clicked_aux(CipherAuxFunctionsArgument * args) {
    CipherArguments cipher_args = get_cipher_arguments_from_args(args);    
    WindowFunctionsHolder * functions = args->functions;
    functions->on_decrypt_clicked(cipher_args);
}
void on_sign_clicked_aux(CipherAuxFunctionsArgument * args) {
    CipherArguments cipher_args = get_cipher_arguments_from_args(args);    
    WindowFunctionsHolder * functions = args->functions;
    functions->on_sign_clicked(cipher_args);
}
void on_check_clicked_aux(CipherAuxFunctionsArgument * args) {
    CipherArguments cipher_args = get_cipher_arguments_from_args(args);    
    WindowFunctionsHolder * functions = args->functions;
    functions->on_check_clicked(cipher_args);
}

void create_main_window(GtkApplication * app, WindowFunctionsHolder * functions) {

    GtkBuilder * builder = gtk_builder_new_from_file(GLADE_UI_FILE_NAME);
    if(builder == NULL) {
    	printf("Null builder returned => exit\n");
        exit(1);
    }

    GtkWidget * window;
    
    GtkWidget * btnEncrypt;
    GtkWidget * btnDecrypt;
    GtkWidget * btnSign;
    GtkWidget * btnCheck;

    GtkWidget * entFileSrc;
    GtkWidget * entFileRes;
    GtkWidget * entPassword;
    GtkWidget * btnFileChoose;
    GtkWidget * btnFileChooseNew;

    window = GTK_WIDGET(gtk_builder_get_object(builder, "window"));
    btnEncrypt = GTK_WIDGET(gtk_builder_get_object(builder, "btnEncrypt"));
    btnDecrypt = GTK_WIDGET(gtk_builder_get_object(builder, "btnDecrypt"));
    btnSign = GTK_WIDGET(gtk_builder_get_object(builder, "btnSign"));
    btnCheck = GTK_WIDGET(gtk_builder_get_object(builder, "btnCheck"));
    entFileSrc = GTK_WIDGET(gtk_builder_get_object(builder, "entFileSrc"));
    entFileRes = GTK_WIDGET(gtk_builder_get_object(builder, "entFileRes"));
    entPassword = GTK_WIDGET(gtk_builder_get_object(builder, "entPassword"));
    btnFileChoose = GTK_WIDGET(gtk_builder_get_object(builder, "btnFileChoose"));
    btnFileChooseNew = GTK_WIDGET(gtk_builder_get_object(builder, "btnFileChooseNew"));

    g_signal_connect(G_OBJECT(window), "destroy", gtk_main_quit, NULL);

    CipherAuxFunctionsArgument * args_encrypt_decrypt = malloc(sizeof(CipherAuxFunctionsArgument));
    args_encrypt_decrypt->functions = functions;
    args_encrypt_decrypt->filenamesrc_entry = GTK_ENTRY(entFileSrc);
    args_encrypt_decrypt->filenameres_entry = GTK_ENTRY(entFileRes);
    args_encrypt_decrypt->password_entry = GTK_ENTRY(entPassword);
    args_encrypt_decrypt->window = GTK_WINDOW(window);
    args_encrypt_decrypt->app = app;
	g_signal_connect_swapped(GTK_BUTTON(btnEncrypt), "clicked", G_CALLBACK(on_encrypt_clicked_aux), args_encrypt_decrypt);
    g_signal_connect_swapped(GTK_BUTTON(btnDecrypt), "clicked", G_CALLBACK(on_decrypt_clicked_aux), args_encrypt_decrypt);
    g_signal_connect_swapped(GTK_BUTTON(btnSign), "clicked", G_CALLBACK(on_sign_clicked_aux), args_encrypt_decrypt);
    g_signal_connect_swapped(GTK_BUTTON(btnCheck), "clicked", G_CALLBACK(on_check_clicked_aux), args_encrypt_decrypt);

    OpenDialogArguments * args_new = malloc(sizeof(OpenDialogArguments));
    args_new->window = window;
    args_new->entry = entFileRes;
    OpenDialogArguments * args = malloc(sizeof(OpenDialogArguments));
    args->window = window;
    args->entry = entFileSrc;
    g_signal_connect_swapped(GTK_BUTTON(btnFileChooseNew), "clicked", G_CALLBACK(open_dialog_file_new), args_new);
    g_signal_connect_swapped(GTK_BUTTON(btnFileChoose), "clicked", G_CALLBACK(open_dialog_file_open), args);

    gtk_widget_show_all(window);

    gtk_main();

    free(args);
    free(args_new);
    free(functions);
}

int create_gtk_app(WindowFunctionsHolder * functions, int argc, char ** argv) {
    GtkApplication * app;
    int status;

	gtk_init(&argc, & argv);

    app = gtk_application_new("ghislain.com", G_APPLICATION_FLAGS_NONE);

    g_signal_connect(app, "activate", G_CALLBACK(create_main_window), functions);

    status = g_application_run(G_APPLICATION(app), argc, argv);

    g_object_unref(app);
    return status;
}









void close_app() {
    gtk_main_quit();
}