#ifndef _DIALOGS_
#define _DIALOGS_

#include <gtk/gtk.h>
#include <stdlib.h>
#include <stdio.h>

/*Fonctions et types utilisees pour la creation des boites de dialogue*/
typedef struct OpenDialogArguments {
	GtkWidget * window;
	GtkWidget * entry;
} OpenDialogArguments;

void open_dialog_file_new(OpenDialogArguments * args);
void open_dialog_file_open(OpenDialogArguments * args);
void open_dialog_file(OpenDialogArguments * args, GtkFileChooserAction action);

void open_dialog_message_default(GtkWindow * parent, char * titre, char * message, void (*on_ok)(), void (*on_annuler)());
void open_dialog_message(GtkWindow * parent, char * titre, char * message, char * ok_label, void (*on_ok)(), char * annuler_label, void (*on_annuler)());

#endif