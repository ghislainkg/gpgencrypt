#include <stdio.h>
#include <stdlib.h>
#include <gcrypt.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <fcntl.h>

#include "gpg/aes256.h"
#include "gpg/keysmanager.h"
#include "utils/filemanager.h"

#include "linker.h"

int main(int argc, char *argv[]) {
	create_app(argc, argv);
	return 0;
}

