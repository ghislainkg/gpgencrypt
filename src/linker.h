#include <stdlib.h>

#include "gpg/aes256.h"
#include "gpg/keysmanager.h"
#include "gpg/signing.h"
#include "utils/filemanager.h"
#include "utils/errors.h"

#include "gtk/windowui.h"
#include "gtk/dialogs.h"

void on_encrypt_clicked(CipherArguments args);
void on_decrypt_clicked(CipherArguments args);
void on_sign_clicked();
void on_check_clicked();

void create_app(int argc, char ** argv);


void onQuit();