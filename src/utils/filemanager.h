#ifndef _FILE_MANAGER_
#define _FILE_MANAGER_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <fcntl.h>

#include "../gpg/aes256.h"
#include "../gpg/keysmanager.h"

#include "errors.h"

/* On peut faire la lecture simple d'un fichier
*/
size_t get_text_from_file(char * filename, char ** buffer, int * error);
/* Ainsi que l'ecriture simple d'un fichier
*/
void write_text_in_file(char * filename, char * text, size_t text_len, int * error);

/* On peut aussi lire un fichier encrypte par AES256.
- On obtient alors le texte crypte (stocker dans *text)
- Le salt initialement utilise pour crypte ce fichier
- la matrice initialement utilise pour crypte ce fichier
*/
size_t read_data_from_fileaes256(char * filename, char ** text, char ** salt, void ** matrice, int * error);

/* On peut ecrire un texte crypte ainsi que les informations necessaire 
pour le decrypte si on a le mot de passe dans un fichier.
Donc on ecrira dans ce fichier a la suite :
- la matrice (de taille aes256.h::AES256_BLOCK_SIZE)
- le salt (de taille keysmanager.h::KDF_SALT_SIZE)
- le texte crypte
*/
void write_data_in_fileaes256(char * filename, char * text, char * salt, void * matrice, size_t text_len, int * error);


size_t read_hash_from_filesign(char * filename, char ** hash, char ** salt, int * error);

void write_hash_in_filesign(char * filename, char * hash, size_t hash_len, char * salt, int * error);

#endif