#include "errors.h"

char * getMessageForCode(int code) {
	switch(code) {

		case ERROR_NO :
			return "";

		case ERROR_CANNOT_OPEN_FILE :
			return "Impossible d'ouvrir le fichier";

		case ERROR_NOT_A_FILE :
			return "Ceci n'est pas un fichier";

		case ERROR_CANNOT_WRITE_FILE :
			return "Impossible d'ecrire le fichier";

		case ERROR_CANNOT_START_PROCESS :
			return "Impossible de commencer le processus";

		case ERROR_CANNOT_DECRYPT :
			return "Impossible de decripter";

		case ERROR_CANNOT_ENCRYPT :
			return "Impossible de crypter";

		case ERROR_CANNOT_PROCESS_KEY :
			return "Impossible de generer la cle";

		case ERROR_CANNOT_USE_KEY :
			return "Impossible d'utiliser cette cle";

		case ERROR_CANNOT_USE_MATRIX :
			return "Impossible d'utiliser cette matrice";

		case ERROR_NO_FILE_SPECIFIED :
			return "Aucun fichier specifie";

		default:
		return "Erreur inconnue";
	}
}