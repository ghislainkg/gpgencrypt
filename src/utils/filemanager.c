#include "filemanager.h"

#define TEST_EMPTYNESS(return_val) \
	if(memcmp(filename, "", strlen(filename)) == 0) { \
		*error = ERROR_NO_FILE_SPECIFIED; \
		return return_val; \
	}

int is_file(struct stat st) {
	if((S_IFMT & st.st_mode) == S_IFREG){
		return 1;
	}
	return 0;
}

size_t get_file_size(int fd, int *error) {
	// lire les statistiques du fichier pour en tirer sa taille ... 
	struct stat * filestat = malloc(sizeof(struct stat));
	memset(filestat, 0, sizeof(struct stat));
	int res_filestat = fstat(fd, filestat); 
	if(res_filestat == -1) {
		printf("Fail to read file stat\n");
		*error = ERROR_CANNOT_OPEN_FILE;
		return 0;
	}
	if(!is_file(*filestat)) {
		*error = ERROR_NOT_A_FILE;
		return 0;
	}
	return filestat->st_size;
}

size_t get_text_from_file(char * filename, char ** buffer, int * error) {
	*error = ERROR_NO;
	
	TEST_EMPTYNESS(0);

	int fd = open(filename, O_RDWR);
	if(fd == -1) {
		perror("Fail to open file for reading");
		*error = ERROR_CANNOT_OPEN_FILE;
		return 0;
	}

	size_t file_size = get_file_size(fd, error);
	if(*error != ERROR_NO)
		return 0;

	*buffer = malloc(file_size);

	size_t len = read(fd, *buffer, file_size);
	if(len == -1) {
		perror("Fail to read file");
		*error = ERROR_CANNOT_OPEN_FILE;
		return 0;
	}
	else if(len != file_size) {
		printf("PROBLEME DE LECTURE\n");
		*error = ERROR_CANNOT_OPEN_FILE;
		return 0;
	}

	close(fd);
	return file_size;
}

size_t read_data_from_fileaes256(char * filename, char ** text, char ** salt, void ** matrice, int * error) {
	*error = ERROR_NO;

	TEST_EMPTYNESS(0);

	int fd = open(filename, O_RDONLY);
	if(fd == -1) {
		perror("Fail to open file for writing");
		*error = ERROR_CANNOT_OPEN_FILE;
		return 0;
	}

	size_t file_size = get_file_size(fd, error);
	if(*error != ERROR_NO)
		return 0;

	*matrice = malloc(AES256_BLOCK_SIZE);
	size_t len = read(fd, *matrice, AES256_BLOCK_SIZE);
	if(len == -1) {
		perror("Fail to read file 1");
		*error = ERROR_CANNOT_OPEN_FILE;
		return 0;
	}

	*salt = malloc(SALT_SIZE);
	len = read(fd, *salt, SALT_SIZE);
	if(len == -1) {
		perror("Fail to read file 2");
		*error = ERROR_CANNOT_OPEN_FILE;
		return 0;
	}

	size_t text_len = file_size - AES256_BLOCK_SIZE - SALT_SIZE;
	*text = malloc(text_len);
	len = read(fd, *text, text_len);
	if(len == -1) {
		perror("Fail to read file 3");
		*error = ERROR_CANNOT_OPEN_FILE;
		return 0;
	}
	else if(len != text_len) {
		printf("PROBLEME DE LECTURE\n");
		*error = ERROR_CANNOT_OPEN_FILE;
		return 0;
	}

	close(fd);
	return text_len;
}

void write_text_in_file(char * filename, char * text, size_t text_len, int * error) {
	*error = ERROR_NO;

	TEST_EMPTYNESS();

	int fd = open(filename, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if(fd == -1) {
		perror("Fail to open file for writing");
		*error = ERROR_CANNOT_WRITE_FILE;
		return;
	}

	size_t len = write(fd, text, text_len);
	if(len == -1) {
		perror("Fail to write file");
		*error = ERROR_CANNOT_WRITE_FILE;
		return;
	}
	close(fd);
}

void write_data_in_fileaes256(char * filename, char * text, char * salt, void * matrice, size_t text_len, int * error) {
	*error = ERROR_NO;

	TEST_EMPTYNESS();

	int fd = open(filename, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if(fd == -1) {
		perror("Fail to open file for writing");
		*error = ERROR_CANNOT_WRITE_FILE;
		return;
	}

	size_t len = write(fd, matrice, AES256_BLOCK_SIZE);
	if(len == -1) {
		perror("Fail to write file");
		*error = ERROR_CANNOT_WRITE_FILE;
		return;
	}

	len = write(fd, salt, SALT_SIZE);
	if(len == -1) {
		perror("Fail to write file");
		*error = ERROR_CANNOT_WRITE_FILE;
		return;
	}

	len = write(fd, text, text_len);
	if(len == -1) {
		perror("Fail to write file");
		*error = ERROR_CANNOT_WRITE_FILE;
		return;
	}
	close(fd);
}

size_t read_hash_from_filesign(char * filename, char ** hash, char ** salt, int * error) {
	*error = ERROR_NO;

	TEST_EMPTYNESS(0);

	int fd = open(filename, O_RDONLY);
	if(fd == -1) {
		perror("Fail to open file for writing");
		*error = ERROR_CANNOT_OPEN_FILE;
		return 0;
	}

	size_t file_size = get_file_size(fd, error);
	if(*error != ERROR_NO)
		return 0;

	*salt = malloc(SALT_SIZE);
	size_t len = read(fd, *salt, SALT_SIZE);
	if(len == -1) {
		perror("Fail to read file 2");
		*error = ERROR_CANNOT_OPEN_FILE;
		return 0;
	}

	size_t hash_len = file_size - SALT_SIZE;
	*hash = malloc(hash_len);
	len = read(fd, *hash, hash_len);
	if(len == -1) {
		perror("Fail to read file 3");
		*error = ERROR_CANNOT_OPEN_FILE;
		return 0;
	}
	else if(len != hash_len) {
		printf("PROBLEME DE LECTURE\n");
		*error = ERROR_CANNOT_OPEN_FILE;
		return 0;
	}

	close(fd);
	return hash_len;
}

void write_hash_in_filesign(char * filename, char * hash, size_t hash_len, char * salt, int * error) {
	*error = ERROR_NO;

	TEST_EMPTYNESS();

	int fd = open(filename, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	if(fd == -1) {
		perror("Fail to open file for writing");
		*error = ERROR_CANNOT_WRITE_FILE;
		return;
	}

	size_t len = write(fd, salt, SALT_SIZE);
	if(len == -1) {
		perror("Fail to write file");
		*error = ERROR_CANNOT_WRITE_FILE;
		return;
	}

	len = write(fd, hash, hash_len);
	if(len == -1) {
		perror("Fail to write file");
		*error = ERROR_CANNOT_WRITE_FILE;
		return;
	}

	close(fd);
}
