#include "linker.h"

#define ERROR_HANDLING \
	if(error != ERROR_NO) { \
		char * message = getMessageForCode(error); \
		open_dialog_message_default(args.window, "Erreur", message, NULL, NULL); \
		return ; \
	}

void on_encrypt_clicked(CipherArguments args) {
	int error;
	// On prepare des pointeurs pour le texte a crypte
	char * texte_crypte;
	size_t texte_crypte_len;

	// On lit le texte contenu dans le fichier a crypter
	char * text_clair;
	size_t text_clair_len = get_text_from_file(args.filenamesrc, &text_clair, &error);
	ERROR_HANDLING

	// On genere un salt pour la derivation de cle
	char * salt = malloc(SALT_SIZE);
	gcry_create_nonce(salt, SALT_SIZE);

	// On genere une matrice qui nous servira au cryptage AES256
	char * matrice = malloc(AES256_BLOCK_SIZE);
	gcry_create_nonce(matrice, AES256_BLOCK_SIZE);
	
	// On derive une cle a partir du mot de passe
	void * cle = generate_cle(args.password, AES256_KEY_SIZE, salt, SALT_SIZE, &error);
	ERROR_HANDLING

	// On ouvre un cipher avec notre cle et notre matrice qui nous servira crypte
	gcry_cipher_hd_t handle;
	lance_processus_aes256(&handle, cle, matrice, &error);
	ERROR_HANDLING

	// On encrypte le texte
	texte_crypte_len = encryptage_aes256(handle, text_clair, text_clair_len, &texte_crypte, &error);
	ERROR_HANDLING

	// On ecrit le texte crypte dans un fichier avec les informations necessaires pour le decrypter
	// (Sauf le mot de passe bien-sur)
	write_data_in_fileaes256(args.filenameres, texte_crypte, salt, matrice, texte_crypte_len, &error);
	ERROR_HANDLING

	// On ferme notre cipher et on libere la memoire
	gcry_cipher_close(handle);
	free(texte_crypte);
	free(text_clair);
	free(salt);
	free(matrice);
	free(cle);

	char * message_fin = "Fin de l'encryptage";
	char * titre_fin = "Fin";
	open_dialog_message(args.window, titre_fin, message_fin, "Ok", NULL, "Quitter", onQuit);
}
void on_decrypt_clicked(CipherArguments args) {
	int error;
	// On prepare des pointeurs pour le salt, la matrice et le texte crypte
	char * salt;
	void * matrice;
	char * texte_crypte;

	// On recupere le contenu du fichier a crypter :
	// matrice, salt et texte crypte
	size_t texte_crypte_len = read_data_from_fileaes256(args.filenamesrc, &texte_crypte, &salt, &matrice, &error);
	ERROR_HANDLING

	// On genere la cle a partir du mot de passe fourni et du salt
	void * cle = generate_cle(args.password, AES256_KEY_SIZE, salt, SALT_SIZE, &error);
	ERROR_HANDLING

	// On ouvre un cipher ave notre cle et notre matrice
	gcry_cipher_hd_t handle;
	lance_processus_aes256(&handle, cle, matrice, &error);
	ERROR_HANDLING

	// On decripte le texte crypte
	char * text_clair = malloc(strlen(texte_crypte));
	decryptage_aes256(handle, texte_crypte, texte_crypte_len, text_clair, &texte_crypte_len, &error);
	ERROR_HANDLING

	// On ecrit le texte clair dans un fichier
	write_text_in_file(args.filenameres, text_clair, texte_crypte_len, &error);
	ERROR_HANDLING

	// On ferme le cipher et on libere la memoire
	gcry_cipher_close(handle);
	free(salt);
	free(matrice);
	free(texte_crypte);
	free(cle);
	free(text_clair);

	char * message_fin = "Fin du decriptage";
	char * titre_fin = "Fin";
	open_dialog_message(args.window, titre_fin, message_fin, "Ok", NULL, "Quitter", onQuit);
}
void on_sign_clicked(CipherArguments args) {
	int error;

	char * salt = malloc(SALT_SIZE);
	gcry_create_nonce(salt, SALT_SIZE);

	void * cle = generate_cle(args.password, SIGN_KEY_SIZE, salt, SALT_SIZE, &error);
	ERROR_HANDLING

	gcry_mac_hd_t mac;
	lancer_processus_signatureverif(&mac, cle, &error);
	ERROR_HANDLING

	char * text;
	size_t text_len = get_text_from_file(args.filenamesrc, &text, &error);
	ERROR_HANDLING

	char * hash = malloc(text_len);
	size_t hashlen = text_len;
	signer(mac, text, text_len, hash, &hashlen, &error);

	write_hash_in_filesign(args.filenameres, hash, hashlen, salt, &error);
	ERROR_HANDLING

	gcry_mac_close(mac);
	free(salt);
	free(cle);
	free(text);
	free(hash);
}
void on_check_clicked(CipherArguments args) {
	int error;

	char * salt;
	char * hash;
	size_t hashlen = read_hash_from_filesign(args.filenameres, &hash, &salt, &error);
	ERROR_HANDLING

	void * cle = generate_cle(args.password, SIGN_KEY_SIZE, salt, SALT_SIZE, &error);
	ERROR_HANDLING

	gcry_mac_hd_t mac;
	lancer_processus_signatureverif(&mac, cle, &error);
	ERROR_HANDLING

	char * text;
	size_t text_len = get_text_from_file(args.filenamesrc, &text, &error);
	ERROR_HANDLING

	int ok = verifier(mac, text, text_len, hash, hashlen, &error);
	ERROR_HANDLING

	if(ok) {
		char * message_fin = "Le fichier est authentique";
		char * titre_fin = "Check";
		open_dialog_message(args.window, titre_fin, message_fin, "Ok", NULL, "Quitter", onQuit);
	}
	else {
		char * message_fin = "Le fichier n'est pas authentique";
		char * titre_fin = "Check";
		open_dialog_message(args.window, titre_fin, message_fin, "Ok", NULL, "Quitter", onQuit);
	}

	gcry_mac_close(mac);
	free(salt);
	free(hash);
	free(text);
}

void create_app(int argc, char ** argv) {
	WindowFunctionsHolder * window_functions = create_window_functions();
	window_functions_set_on_encrypt_clicked(window_functions, on_encrypt_clicked);
	window_functions_set_on_decrypt_clicked(window_functions, on_decrypt_clicked);
	window_functions_set_on_sign_clicked(window_functions, on_sign_clicked);
	window_functions_set_on_check_clicked(window_functions, on_check_clicked);

	create_gtk_app(window_functions, argc, argv);
}









void onQuit() {
	close_app();
}