#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gcrypt.h>

#define SALT_SIZE 50
#define KEY_SIZE 50

void * generate_key(char * password);

int main(int argc, char const *argv[]) {
	char * data = "watashi no namae wa ghislain desu !";
	int datalen = strlen(data);

	gcry_mac_hd_t mac;

	gcry_error_t err = gcry_mac_open(&mac, GCRY_MAC_HMAC_SHA512, 0, NULL);
	if (err) {
		fprintf(stderr, "mac_open during encryption: %s/%s\n", gcry_strsource(err), gcry_strerror(err));
		return 1;
	}

	char * password = "01234";
	void * cle = generate_key(password);

	err = gcry_mac_setkey(mac, cle, KEY_SIZE);
	if (err) {
		fprintf(stderr, "mac_setkey during encryption: %s/%s\n", gcry_strsource(err), gcry_strerror(err));
		return 1;
	}

	err = gcry_mac_write(mac, data, datalen);
	if (err) {
		fprintf(stderr, "mac_write during encryption: %s/%s\n", gcry_strsource(err), gcry_strerror(err));
		return 1;
	}

	char * hash = malloc(100);
	size_t hashlen = 100;
	err = gcry_mac_read(mac, hash, &hashlen);
	if (err) {
		fprintf(stderr, "mac_read during encryption: %s/%s\n", gcry_strsource(err), gcry_strerror(err));
		return 1;
	}

	/*printf("HASH : %s ||\n", hash);
	printf("HASH SIZE : %lu\n", hashlen);*/
	for (int i = 0; i < hashlen; ++i)
	{
		printf("%c", hash[i]);
	}
	printf("\n");





	gcry_mac_close(mac);

	gcry_mac_hd_t mac2;


	err = gcry_mac_open(&mac2, GCRY_MAC_HMAC_SHA512, 0, NULL);
	if (err) {
		fprintf(stderr, "mac_open during encryption: %s/%s\n", gcry_strsource(err), gcry_strerror(err));
		return 1;
	}

	err = gcry_mac_setkey(mac2, cle, KEY_SIZE);
	if (err) {
		fprintf(stderr, "mac_setkey during encryption: %s/%s\n", gcry_strsource(err), gcry_strerror(err));
		return 1;
	}


	err = gcry_mac_write(mac2, data, datalen);
	if (err) {
		fprintf(stderr, "mac_write during encryption: %s/%s\n", gcry_strsource(err), gcry_strerror(err));
		return 1;
	}

	err = gcry_mac_verify(mac2, hash, hashlen);
	if(err) {
		if(err == GPG_ERR_CHECKSUM) {
			printf("INVALID VERIFICATION\n");
			return 0;
		}
		else {
			printf("CHECKSUM = %d\n", GPG_ERR_CHECKSUM);
			printf("erreur = %d\n", err);
			fprintf(stderr, "HMAC verification failed: %s/%s\n", gcry_strsource(err), gcry_strerror(err));
			return 1;
		}
	}
	else {
		printf("VERIFICATION VALID\n");
	}

	return 0;
}


void * generate_key(char * password) {
	char * salt = malloc(SALT_SIZE);
	gcry_create_nonce(salt, SALT_SIZE);
	
	void * cle = malloc(KEY_SIZE);
	int nbr_iteration = 5;
	gpg_error_t error_key_gen = gcry_kdf_derive(
							password, // Le mot de passe
							strlen(password), // La taille du mot de passe
							GCRY_KDF_PBKDF2, // Algorithme de derivation de cle utilise
							GCRY_MD_SHA512, // Le sous algorithme utilisé
							salt, // Le paramètre salt
							SALT_SIZE, // La taille du paramètre salt
							nbr_iteration, // Le nombre d'itération de l'algorithme
							KEY_SIZE, // La taille voulu de la clé
							cle); // L'endroit où stocker la clé
	if(error_key_gen > 0) {
		// S'il y a des erreurs
		printf("Fail to generate key : %s\n", gcry_strerror(error_key_gen));
		exit(1);
	}
	return cle;
}